/*
Name: Seegal Panchal
ID:   1016249
Date: 03/05/2018

This file contains the function prototypes
for any functions used in other programs
during this assignment, most commonly init_aim.c

*/

#ifndef AIM_H
#define AIM_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

struct student_struct {
   char last_name[64];
   char first_name[64];
   char middle_names[64];
   int id;
   char major[64];
};

struct course_struct {
   char code[10];
   char term[6];
   char title[128];
   char instructor[64];
};

struct student_course_struct {
   int student_id;
   char course_code[10];
   char term[6];
   char registration[4];
   int grade;
};

struct linked_list {
  struct student_course_struct *student_course;
  struct linked_list *next;
};

struct linked_list *new_item(struct student_course_struct *student_course);
void push(struct linked_list **list, struct linked_list *toAdd);
void free_list(struct linked_list *list);

void print_all_students();

void register_student();
void register_student2();
void sort();
int compar_id(const void *a, const void *b);

void print_student(struct student_struct *student);
void print_course(struct course_struct *course);
void print_student_course(struct student_course_struct *student_course);

void print_class_schedule(struct student_struct *student, struct linked_list *list);

struct student_struct *get_student_info(int student_id);
struct linked_list *schedule_list(char *term, int id);
struct course_struct *get_course_info(char *course_code);


void schedule();
void add();

/* ======================== HELPER SELF MADE =========================== */

void flush(char var[]);
int size_file(char *fileName);
struct student_struct *rnl_student_struct(struct student_struct *student);
struct course_struct *rnl_course_struct(struct course_struct *course);
struct student_course_struct *rnl_student_course_struct(struct student_course_struct *student_course);

#endif
