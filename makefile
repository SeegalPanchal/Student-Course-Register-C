all: aim init_aim register register2 sort schedule add class_schedule class_list grade report

aim: aim.c aim.h
	gcc -ansi -Wall -pedantic -include aim.h -c aim.c -o aim.o

init_aim: init_aim.c aim.c aim.h
	gcc -ansi -Wall -pedantic -include aim.h -c init_aim.c -o init_aim.o
	gcc -ansi -Wall -pedantic -include aim.h aim.o init_aim.o -o init_aim

register: register.c aim.c aim.h
	gcc -ansi -Wall -pedantic -include aim.h -c register.c -o register.o
	gcc -ansi -Wall -pedantic -include aim.h aim.o register.o -o register

register2: register2.c aim.c aim.h
	gcc -ansi -Wall -pedantic -include aim.h -c register2.c -o register2.o
	gcc -ansi -Wall -pedantic -include aim.h aim.o register2.o -o register2

sort: sort.c aim.c aim.h
	gcc -ansi -Wall -pedantic -include aim.h -c sort.c -o sort.o
	gcc -ansi -Wall -pedantic -include aim.h aim.o sort.o -o sort

schedule: schedule.c aim.c aim.h
	gcc -ansi -Wall -pedantic -include aim.h -c schedule.c -o schedule.o
	gcc -ansi -Wall -pedantic -include aim.h aim.o schedule.o -o schedule

add: add.c aim.c aim.h
	gcc -ansi -Wall -pedantic -include aim.h -c add.c -o add.o
	gcc -ansi -Wall -pedantic -include aim.h aim.o add.o -o add

class_schedule: class_schedule.c aim.c aim.h
	gcc -ansi -Wall -pedantic -include aim.h -c class_schedule.c -o class_schedule.o
	gcc -ansi -Wall -pedantic -include aim.h aim.o class_schedule.o -o class_schedule

class_list: class_list.c aim.c aim.h
	gcc -ansi -Wall -pedantic -include aim.h -c class_list.c -o class_list.o
	gcc -ansi -Wall -pedantic -include aim.h aim.o class_list.o -o class_list

grade: grade.c aim.c aim.h
	gcc -ansi -Wall -pedantic -include aim.h -c grade.c -o grade.o
	gcc -ansi -Wall -pedantic -include aim.h aim.o grade.o -o grade

report: report.c aim.c aim.h
	gcc -ansi -Wall -pedantic -include aim.h -c report.c -o report.o
	gcc -ansi -Wall -pedantic -include aim.h aim.o report.o -o report

clean:
	rm -f *.o init_aim register register2 sort schedule add class_schedule class_list grade report

delete:
	rm -i *.bin
