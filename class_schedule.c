#include "aim.h"

int main(int argc, char **argv) {
  if (argc != 3) {
    fprintf( stderr, "Usage: %s [id] [term]\n", argv[0] );
    exit(-1);
  } else {

    /* set up the inputs */
    int id = atoi(argv[1]);
    char *term = argv[2];

    /* find the student through the id inputted*/
    struct student_struct *student = get_student_info(id);
    /* if the student exists */

    if (student) {
      struct linked_list *list = NULL;
      list = schedule_list(term, id);
      print_class_schedule(student, list);
      free_list(list);
    }
  }
  return 0;
}
