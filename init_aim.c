#include "aim.h"

int main() {
  FILE *fp;

  /* make students.bin */
  fp = fopen("students.bin", "wb+");
  fclose(fp);

  /* make courses.bin */
  fp = fopen("courses.bin", "wb+");
  fclose(fp);

  /* make student_course.bin */
  fp = fopen("student_course.bin", "wb+");
  fclose(fp);
  return 0;
}
