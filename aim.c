#include "aim.h"
#include <time.h>

void register_student() {

  int max;
  FILE *fp;
  struct student_struct *student;

  char temp[64];

  /* open the file and set the minimum student id
  allocate memory for the student struct */
  max = 0;
  fp = fopen("students.bin", "rb+");
  student = malloc(sizeof(struct student_struct));
  /* read through the file, and get the max value*/
  while (!feof(fp)) {
    fread(student, sizeof(struct student_struct), 1, fp);
    /* find the max student id */
    if (student->id > max) {
      max = student->id;
    }
  }

  printf("Last name: ");
  fgets(temp, 64, stdin);
  strncpy(student->last_name, temp, 63);
  flush(temp);

  printf("First name: ");
  fgets(temp, 64, stdin);
  strncpy(student->first_name, temp, 63);
  flush(temp);

  printf("Middle names: ");
  fgets(temp, 64, stdin);
  strncpy(student->middle_names, temp, 63);
  flush(temp);

  printf("Major: ");
  fgets(temp, 64, stdin);
  strncpy(student->major, temp, 63);
  flush(temp);

  student->id = max+1;

  rnl_student_struct(student);

  fwrite(student, sizeof(struct student_struct), 1, fp);

  free(student);

  fclose(fp);
}

void register_student2() {
  int max;
  FILE *fp;
  struct student_struct *student;

  /* open the file and set the minimum student id
  allocate memory for the student struct */
  max = 0;
  fp = fopen("students.bin", "rb+");
  student = malloc(sizeof(struct student_struct));
  /* read through the file, and get the max value*/
  while (!feof(fp)) {
    fread(student, sizeof(struct student_struct), 1, fp);
    /* find the max student id */
    if (student->id > max) {
      max = student->id;
    }
  }

  printf("Last name: ");
  fgets(student->last_name, sizeof(student->last_name), stdin);

  printf("First name: ");
  fgets(student->first_name, sizeof(student->first_name), stdin);

  printf("Middle names: ");
  fgets(student->middle_names, sizeof(student->middle_names), stdin);

  printf("Major: ");
  fgets(student->major, sizeof(student->major), stdin);

  srand(time(NULL));
  student->id = rand() % 100;

  student = rnl_student_struct(student);

  fwrite(student, sizeof(struct student_struct), 1, fp);

  free(student);

  fclose(fp);
}

void sort() {
  FILE *fp;
  int num_students;
  struct student_struct *student_array;

  num_students = size_file("students.bin") / sizeof(struct student_struct);
  fp = fopen("students.bin", "rb+");

  if (fp != NULL) {
    student_array = calloc(num_students, sizeof(struct student_struct));
    /* read the file into the array */
    fread(student_array, sizeof(struct student_struct), num_students, fp);
    /* sort the array */
    qsort(student_array, num_students, sizeof(struct student_struct), compar_id);
    /* go to beginning of file */
    fseek(fp, 0, SEEK_SET);
    /* write the array to the file */
    fwrite(student_array, sizeof(struct student_struct), num_students, fp);
    /* go to beginning of file */
    fseek(fp, 0, SEEK_SET);
    /* free the allocated memory */
    free(student_array);
  }

  fclose(fp);
}

int compar_id(const void *a, const void *b) {
    const struct student_struct *student_a, *student_b;

    student_a = a;
    student_b = b;

    return ((student_a->id)-(student_b->id));
}

void schedule() {
  FILE *fp;
  struct course_struct *course;

  char temp[128];

  /* open the file and set the minimum student id
  allocate memory for the student struct */
  fp = fopen("courses.bin", "a");

  /* open file direcly in append mode */
    course = malloc(sizeof(struct course_struct));

  printf("Code: ");
  fgets(temp, 10, stdin);
  strncpy(course->code, temp, 9);
  flush(temp);

  printf("Term: ");
  fgets(temp, 6, stdin);
  strncpy(course->term, temp, 5);
  flush(temp);

  printf("Title: ");
  fgets(temp, 128, stdin);
  strncpy(course->title, temp, 127);
  flush(temp);

  printf("Instructor: ");
  fgets(temp, 64, stdin);
  strncpy(course->instructor, temp, 63);
  flush(temp);

  /* remove any new lines from the course structure */
  course = rnl_course_struct(course);

  fwrite(course, sizeof(struct course_struct), 1, fp);

  free(course);

  fclose(fp);
}

void add() {
  FILE *fp;
  struct student_course_struct *student_course;

  char temp[10];

  /* open the file and set the minimum student id
  allocate memory for the student struct */
  fp = fopen("student_course.bin", "a");

  /* open file direcly in append mode */
  student_course = malloc(sizeof(struct student_course_struct));

  /* allow student to input his ID here */
  printf("ID: ");
  scanf("%d", &student_course->student_id);
  flush("");
  /*
  fgets(input_id, sizeof(student_course->student_id) * 3, stdin);
  flush(input_id);
  student_course->student_id = atoi(input_id);
  */
  printf("Course Code: ");
  fgets(temp, 10, stdin);
  strncpy(student_course->course_code, temp, 9);
  flush(temp);

  printf("Term: ");
  fgets(temp, 6, stdin);
  strncpy(student_course->term, temp, 5);
  flush(temp);

  while (strcmp(student_course->registration, "crd") != 0
    && strcmp(student_course->registration, "aud") != 0
    && strcmp(student_course->registration, "CRD") != 0
    && strcmp(student_course->registration, "AUD") != 0
    && strcmp(student_course->registration, "Crd") != 0
    && strcmp(student_course->registration, "Aud") != 0) {
      printf("Registration [CRD or AUD]: ");
      /* get the side the player wants to play (X or O)*/
      fgets(temp, 4, stdin);
      strncpy(student_course->registration, temp, 3);
      flush(temp);
  }

  student_course->grade = -1;


  student_course = rnl_student_course_struct(student_course);

  fwrite(student_course, sizeof(struct student_course_struct), 1, fp);

  free(student_course);

  fclose(fp);
}

/* =========================== CLASS SCHEDULE ================================= */

struct linked_list *schedule_list(char *term, int id) {

  FILE *fp;
  int i = 0;
  int num_students = size_file("student_course.bin") / sizeof(struct student_course_struct);
  struct student_course_struct *student_course;

  struct linked_list *list = NULL;

  fp = fopen("student_course.bin", "rb");
  if (fp != NULL) {
    /* allocate memory to the array */
    student_course = malloc(sizeof(struct student_course_struct));
    /* loop through array looking for term */
    for (i = 0; i < num_students; i++) {
      fread(student_course, sizeof(struct student_course_struct), 1, fp);
      /* if the term and the id match, then return the student */
      if (strcmp(student_course->term, term) == 0 && id == student_course->student_id) {
        struct linked_list *new = new_item(student_course);
        push(&list, new);
      }
    }
    /* if it isn't found, free it */
    free(student_course);
    fclose(fp);
    return list;
  }

  fclose(fp);
  return list;
}

void print_class_schedule(struct student_struct *student, struct linked_list *list) {
  /* print the student */
  printf("Name: %s %s %s\n", student->first_name, student->middle_names, student->last_name);
  printf("ID: %07d\n", student->id);

  /* print the linked list */
  while (list) {
    char *code = list->student_course->course_code;
    struct course_struct *course = get_course_info(code);
    if (course) {
      printf("%s %s\n", course->code, course->title);
      printf("%s\n", course->instructor);
    }
    free(course);
    list = list->next;
  }
  free(student);
}

/* ========================= PRINT ================================= */

void print_student(struct student_struct *student) {
  printf("Name: %s %s %s\n", student->first_name, student->middle_names, student->last_name);
  printf("ID: %07d\n", student->id);
  /*
  printf("Major: %s\n", student->major);
  */
}
void print_student_course(struct student_course_struct *student_course) {
  printf("ID: %07d\n", student_course->student_id);
  printf("Course Code: %s\n", student_course->course_code);
  printf("Term: %s\n", student_course->term);
  printf("Registration: %s\n", student_course->registration);
  printf("Grade: %d\n", student_course->grade);
}

/* ================= GET INFO FROM CERTAIN PARAMETERS ========================= */

struct student_struct *get_student_info(int student_id) {
  struct student_struct *student = malloc(sizeof(struct student_struct));

  int left = 0;
  int right = size_file("students.bin") / sizeof(struct student_struct) -1;

  FILE *fp = fopen("students.bin", "rb");
  while (left <= right) {

    int mid = (left+right) / 2;
    /* start the beginning of the file,
    go to the middle student that we want to read */
    fseek(fp, mid*sizeof(struct student_struct), SEEK_SET);
    /* read the student into our struct */
    fread(student, sizeof(struct student_struct), 1, fp);
    if (student_id == student->id) {
      fclose(fp);
      return student;
    }
    if (student_id < student->id) {
      right = mid-1;
    }
    if (student_id > student->id) {
      left = mid+1;
    }
  }
  fclose(fp);
  free(student);
  return NULL;
}
struct course_struct *get_course_info(char *course_code) {
  FILE *fp;
  int i = 0;
  int num_courses = size_file("courses.bin") / sizeof(struct course_struct);
  struct course_struct *course;

  fp = fopen("courses.bin", "rb");

  if (fp != NULL) {
    /* allocate memory to the array */
    course = malloc(sizeof(struct course_struct));
    /* read the whole file at the same time */

    /* loop through array looking for term */
    for (i = 0; i < num_courses; i++) {
      fread(course, sizeof(struct course_struct), 1, fp);
      /* if the term and the id match, then return the student */
      if (strcmp(course->code, course_code) == 0) {
        fclose(fp);
        return course;
      }
    }
    /* if it isn't found, free it */
    free(course);
  }

  fclose(fp);

  return NULL;
}

/* ============================= LINKED LIST ================================== */

struct linked_list *new_item(struct student_course_struct *student_course) {
  struct linked_list *new;
  new = malloc(sizeof(struct linked_list));
  new->student_course = malloc(sizeof(struct student_course_struct));
  memcpy(new->student_course, student_course, sizeof(struct student_course_struct));
  new->next = NULL;
  return new;
}
void push(struct linked_list **list, struct linked_list *toAdd) {

  struct linked_list *current = (*list);
  struct linked_list *previous = current;
  while (current != NULL) {
    /* if the course code to add appears BEFORE the one we are looking at */
    if (strcmp(toAdd->student_course->course_code, current->student_course->course_code) < 0) {
      /* if what we are looking at is the head of the list
      then add it to the front of the list and return */
      if (current == (*list)) {
        toAdd->next = (*list);
        (*list) = toAdd;
        return;
      }
      /* make the previous node point to the node we want to add */
      previous->next = toAdd;
      /* make the node we are adding point what what previous used to point to */
      toAdd->next = current;
      /* return from the function */
      return;
    }
    /* else increment the list to the next item */
    else {
      /* make the current node the tail */
      previous = current;
      /* check the next node now */
      current = current->next;
    }
  }

  /* being here means current == NULL, but
  previous might not be, if it isn't, that
  means we want to put it at the end of the list*/
  if (previous != NULL) {
    previous->next = toAdd;
    toAdd->next = NULL;
    return;
  }

  /* previous IS null, so
  make the list point to the node
  we want to add and be done with it */
  *list = toAdd;

  /* return out of the function */
  return;
}
void free_list(struct linked_list *list) {
  struct linked_list *ptr;
  while (list) {
    ptr = list;
    list = ptr->next;
    free(ptr->student_course);
    free(ptr);
  }
}

/* =================== HELPER FUNCTIONS - SELF CREATED ======================== */
void print_all_students() {

  int i;
  for (i = 0; i < 100; i++) {
    struct student_struct *temp = get_student_info(i);
    if (temp) {
      print_student(temp);
      free(temp);
    }
  }
  /*
  FILE *fp = fopen("students.bin", "rb");
  struct student_struct *student = malloc(sizeof(struct student_struct));
  while (fread(student, sizeof(struct student_struct), 1, fp)) {
    print_student(student);
  }
  free(student);
  */
}
int size_file(char *fileName) {
  int length = 0;
  FILE *fp = fopen(fileName, "rb");
  if (fp != NULL) {
    fseek(fp, 0, SEEK_END);
    length = ftell(fp);
  }
  fclose(fp);
  return length;
}
void flush(char var[]) {
  /* flush any overflowing input from fgets */
  /* if new line dooesnt exist from previous fgets*/
  if (!strchr(var, '\n')) {
    while (fgetc(stdin)!='\n'); /* discard until new line */
  }
}
/* remove new line for a student structure */
struct student_struct *rnl_student_struct(struct student_struct *student) {
  if (strchr(student->last_name, '\n')) {
    student->last_name[strlen(student->last_name)-1] = '\0';
  } else {
    student->last_name[strlen(student->last_name)] = '\0';
  }

  if (strchr(student->first_name, '\n')) {
    student->first_name[strlen(student->first_name)-1] = '\0';
  } else {
    student->first_name[strlen(student->first_name)] = '\0';
  }

  if (strchr(student->middle_names, '\n')) {
    student->middle_names[strlen(student->middle_names)-1] = '\0';
  } else {
    student->middle_names[strlen(student->middle_names)] = '\0';
  }

  if (strchr(student->major, '\n')) {
    student->major[strlen(student->major)-1] = '\0';
  } else {
    student->major[strlen(student->major)] = '\0';
  }
  return student;
}
/* remove new line for a course structure */
struct course_struct *rnl_course_struct(struct course_struct *course) {

  if(strchr(course->code, '\n')) {
    course->code[strlen(course->code)-1] = '\0';
  } else {
    course->code[strlen(course->code)] = '\0';
  }
  if (strchr(course->term, '\n')) {
    course->term[strlen(course->term)-1] = '\0';
  } else {
    course->term[strlen(course->term)] = '\0';
  }
  if (strchr(course->title, '\n')) {
    course->title[strlen(course->title)-1] = '\0';
  } else {
    course->title[strlen(course->title)] = '\0';
  }
  if (strchr(course->instructor, '\n')) {
    course->instructor[strlen(course->instructor)-1] = '\0';
  } else {
    course->instructor[strlen(course->instructor)] = '\0';
  }
  return course;
}
/* remove the new line from a student course struct */
struct student_course_struct *rnl_student_course_struct(struct student_course_struct *student_course) {
  if (strchr(student_course->course_code, '\n')) {
    student_course->course_code[strlen(student_course->course_code)-1] = '\0';
  } else {
    student_course->course_code[strlen(student_course->course_code)] = '\0';
  }
  if (strchr(student_course->term, '\n')) {
    student_course->term[strlen(student_course->term)-1] = '\0';
  } else {
    student_course->term[strlen(student_course->term)] = '\0';
  }
  if (strchr(student_course->registration, '\n')) {
    student_course->registration[strlen(student_course->registration)-1] = '\0';
  } else {
    student_course->registration[strlen(student_course->registration)] = '\0';
  }
  return student_course;
}
